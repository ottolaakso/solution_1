#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 13:39:03 2020

@author: ottolaakso
"""

import torch
import numpy as np

#Implementation using numpy only
m1 = np.array(range(0,6)).reshape(2,3)
m2 = np.array(range(6,12)).reshape(2,3)
x1 = np.array(range(3)).reshape(3,1)

y = np.matmul((m1, m2), x1)

print(y, y.shape)

#Implementation using pytorch
n1 = torch.arange(0,6).reshape(2,3)
n2 = torch.arange(6,12).reshape(2,3)
z1 = torch.arange(3).reshape(3,1)

w = torch.matmul(torch.stack((n1, n2), 0), z1)

print(w, w.shape)

