#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: ottolaakso

"""

import torch
import numpy as np

m1 = np.array(range(0,6)).reshape(2,3)
m2 = np.array(range(6,12)).reshape(2,3)
x1 = np.array(range(3)).reshape(3,1)

y = (m1, m2) @ x1
y = torch.from_numpy(y)

print(y, y.shape)

