""" Otto Laakso """

import torch

x_train = torch.arange(0,6)
y_train = torch.arange(0,12,2)

lr = 1e-1
n_epochs = 1000

c = torch.randn(1, requires_grad=True)

for epoch in range(n_epochs):
    
    yhat = c * x_train
    error = y_train - yhat
    loss = (error ** 2).mean()
    
    loss.backward()
    
    with torch.no_grad():
        c -= lr * c.grad
        
    c.grad.zero_()
    
print(c)


